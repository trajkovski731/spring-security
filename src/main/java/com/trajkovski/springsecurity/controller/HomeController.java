package com.trajkovski.springsecurity.controller;

import com.trajkovski.springsecurity.entity.User;
import com.trajkovski.springsecurity.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/home")
public class HomeController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping
    public String home() {
        return "home.jsp";
    }

    @PostMapping
    public User addUser(@RequestBody final User user) {
        return userRepository.save(user);
    }


}
